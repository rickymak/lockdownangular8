import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mylogin',
  templateUrl: './mylogin.component.html',
  styleUrls: ['./mylogin.component.scss']
})
export class MyloginComponent implements OnInit {

  constructor( public router: Router,) { }

  ngOnInit() {
  }
  onLoggedin() {

    localStorage.setItem('isLoggedin', 'true');
    this.router.navigate(['/dashboard']);
}
}
