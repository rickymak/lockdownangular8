import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ServiceService } from '../../commonservice/service.service';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']

})
export class SubcategoryComponent implements OnInit {
  constructor(private authService: ServiceService, private formBuilder: FormBuilder) {
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });

    this.addroleformGroup = formBuilder.group({
      roleName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],

    });
    this.subCategoryList();
  }

  @ViewChild('paginator') paginator: MatPaginator;

  @ViewChild('sort') sort: MatSort;


  Data: any = [];
  requestData: any = [];
  getSubList: any;
  returnMinDate: any = new Date();
  imgUrl: any;
  roleName: any;
  MainCategoryName: any;
  MainCategory: any;
  SubCategoryName: any;
  addroleformGroup: FormGroup;
  profileForm: FormGroup;
  RequestData: any = [];
  displayedColumns: string[] = ['Sno', 'MainCategoryName', 'SubCategoryName', 'Image', 'Action'];
  dataSource = new MatTableDataSource();

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(); // create new object
    this.dataSource.sort = this.sort;
    this.dataSource.data = this.RequestData;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  subCategoryList() {
    this.authService.getSubCat().then(
      (result: any) => {
        if (result.status) {

          this.getSubList = result.data;
          for (let i = 0; i < this.getSubList.length; i++) {
            this.requestData = {
              'Sno': i + 1,
              'MainCategoryName': this.getSubList[i]['MainCategory'],
              'SubCategoryName': this.getSubList[i]['Subcategory'],
              'Image': this.getSubList[i]['imageUrl']
            };
            this.Data.push(this.requestData);
            console.log('Get this.Data', this.Data);
          }

          this.dataSource.data = this.Data;
        } else {
          alert('data not found');
        }


      }, (err) => {



      }
    );
  }

  onChange(val) {
    this.MainCategory = val;
  }

  onSelectedFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileForm.get('profile').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('image', this.profileForm.get('profile').value);
    formData.append('MaincategoryID', this.MainCategory);
    formData.append('Subcategory', this.roleName);
    console.log('formData', formData);
    this.authService.createSubCate(formData).then((data: any) => {
      if (data.status) {
          this.subCategoryList();
          //  $('.close').trigger('click');
            this.addroleformGroup.reset();
        } else if (!data.status) {
            alert(data.message);
        }
      },
        error => {
          console.log(error.error);
          alert('Inernal Server error' + JSON.stringify(error.error));
        }
        );
      }
}
