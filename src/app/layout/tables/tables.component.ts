import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ServiceService } from '../../commonservice/service.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss'],
  animations: [routerTransition()]
})
export class TablesComponent implements OnInit {
  constructor(private authService: ServiceService, private formBuilder: FormBuilder) {
    this.profileForm = this.formBuilder.group({
      name: [''],
      profile: ['']
    });

    this.addroleformGroup = formBuilder.group({
      roleName: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],
      MainCategory: ['', Validators.compose([Validators.required, Validators.maxLength(100)])],

    });
    this.MainCategoryList();
  }

  @ViewChild('paginator') paginator: MatPaginator;

  @ViewChild('sort') sort: MatSort;


  Data: any = [];
  requestData: any = [];
  geRoleList: any;
  returnMinDate: any = new Date();
  imgUrl: any;
  roleName: any;
  MainCategory: any;
  addroleformGroup: FormGroup;
  profileForm: FormGroup;
  RequestData: any = [
    { Sno: '01', Code: '11-341 AKH', Count: '14', Date: '01/11/19' },
    { Sno: '02', Code: '11-342 AKH', Count: '08', Date: '02/11/19' },
    { Sno: '03', Code: '11-343 AKH', Count: '14', Date: '03/11/19' },
    { Sno: '04', Code: '16-344 AKH', Count: '74', Date: '04/11/19' },
    { Sno: '05', Code: '11-345 AKH', Count: '52', Date: '05/11/19' },
    { Sno: '06', Code: '11-346 AKH', Count: '89', Date: '06/11/19' },
    { Sno: '07', Code: '14-347 AKH', Count: '21', Date: '07/11/19' },
    { Sno: '08', Code: '11-348 AKH', Count: '51', Date: '08/11/19' },
    { Sno: '09', Code: '11-349 AKH', Count: '31', Date: '09/11/19' },
    { Sno: '10', Code: '11-350 AKH', Count: '42', Date: '10/11/19' },
    { Sno: '11', Code: '13-351 AKH', Count: '19', Date: '11/11/19' },
    { Sno: '12', Code: '11-352 AKH', Count: '40', Date: '12/11/19' },
    { Sno: '13', Code: '12-353 AKH', Count: '90', Date: '14/11/19' },


  ];
  //  displayedColumns: string[] = ['Sno', 'Code', 'Count', 'Date'];
  displayedColumns: string[] = ['Sno', 'CategoryName', 'Image', 'Action'];
  dataSource = new MatTableDataSource();

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  ngOnInit() {
    this.dataSource = new MatTableDataSource(); // create new object
    this.dataSource.sort = this.sort;
    this.dataSource.data = this.RequestData;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  MainCategoryList() {
    this.authService.getMainCat().then(
      (result: any) => {
        if (result.status) {

          this.geRoleList = result.data;
          for (let i = 0; i < this.geRoleList.length; i++) {
            this.requestData = {
              'Sno': i + 1,
              'CategoryName': this.geRoleList[i]['MainCateory'],
              'Image': this.geRoleList[i]['imageUrl']
            };
            this.Data.push(this.requestData);
            console.log('Get this.Data', this.Data);
          }

          this.dataSource.data = this.Data;
        } else {
          alert('data not found');
        }


      }, (err) => {



      }
    );
  }

  onSelectedFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileForm.get('profile').setValue(file);
    }
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('image', this.profileForm.get('profile').value);
    //  formData.append('MaincategoryID', this.MainCategory);
    formData.append('MainCateory', this.roleName);
    console.log('formData', formData);
    this.authService.MainCategory(formData).then((data: any) => {
      if (data.status) {
        // $('.close').trigger('click');
      } else if (!data.status) {
        alert(data.message);
      }

    },
      error => {
        console.log(error.error);
        alert('Inernal Server error' + JSON.stringify(error.error));

      }

    );
  }

  EditMain(action, x) {
    this.roleName = x.MainCateory;
    this.imgUrl = x.imageUrl;
  }

  /**********to get date from calendar(matdatePicker)******************/
  getMinReturn(val) {
    this.returnMinDate = val;
    console.log(val);
  }
}
