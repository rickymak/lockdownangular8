import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { ServiceService } from '../commonservice/service.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    email: any;
    password: any;
    constructor(
        public router: Router, private authService: ServiceService
    ) { }

    ngOnInit() { }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    onLogin() {

        let postParams = {
            username: this.email,
            password: this.password,
        }
        this.authService.login(postParams).then(
            (result: any) => {
                if (result.status) {
                    localStorage.setItem('isLoggedin', 'true');
                    this.router.navigate(['/dashboard']);
                }
                else if (!result.status) {
                    alert(result.message)
                }
            }, (err) => {
            }
        );
    }
}
